# Development tools #

A list of useful development tools for the everyday work. Add them as you find them!

##Web Development##

* **[Bootstrap CheatSheet](http://hackerthemes.com/bootstrap-cheatsheet/)**: Cheat sheet for Bootstrap v4.0.0
* **[Plunker](http://plnkr.co/)**: A web development playground.
* **[HTTPie](https://github.com/jkbrzt/httpie)**: A CLI HTTP client, great for testing APIs.



##Regular Expressions##

* **[RegexOne](http://regexone.com/)**: Dynamic tutorial for learning Regular Expressions. 
* **[Regular Expressions 101](https://regex101.com/)**: Comprehensive tool for writing/testing regular expressions.


##Algorithms##

* **[Algorithm Visualizer](http://algo-visualizer.jasonpark.me/#path=backtracking/knight%27s_tour/basic)**: Great for understanding algorithms for sorting, search, graph traversal etc. visually shows algorithms step by step. 
* **[Data Structures Visualizer](http://visualgo.net/)**: Great for visually understanding traversals on common data structures and graphs.

##Misc##

* **[HTML Encoder/Decoder](http://www.web2generators.com/html-based-tools/online-html-entities-encoder-and-decoder)**: Encode/Decode HTML ``&lt;h1&gt;Hello World&lt;/h1&gt; => <h1>Hello World</h1>``